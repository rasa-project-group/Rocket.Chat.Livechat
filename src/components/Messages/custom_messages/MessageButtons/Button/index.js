import { h , Component } from "preact";
import { createClassName, memo } from '../../../../helpers';
import { css, jsx } from '@emotion/react'
import {update_insert_hide,update_card_offer_actions,send_card_offer_payload} from "../../MessageOffers/Actions/methodes/Offer_actions"
import {custom_Styles} from "./style"
import {Button} from "rmwc"
/** @jsx jsx */
import '@rmwc/button/styles';
import Zoom from "../../../../../icons/video_call.svg";
/**
 * element={"title":String,"payload":""}
 */
export const Custom_Button = memo(
	({
		action,
		element,
		title,
		Style,
		icon,

	}) => (

		<Button raised   css={Style}  label={element.title} onClick={()=>{ update_insert_hide(action,send_card_offer_payload(element,{_hidden:true}),()=>{})}} ></Button>
	))



/**
 * element   { icon: 'icon-videocam', label: TAPi18n.__('Click_to_join'), i18nLabel: 'Click_to_join', method_id: 'joinJitsiCall', params: ''}
 */
 export const Custom_link_Button = memo(
	({
		action,
		element,
		title,
		Style,
		icon,
		data

	}) => (
		<div css={Style}>
		<Button style={{display:"flex" , flexDirection:"row"}}  raised icon={<Zoom></Zoom>} label={(element.i18nLabel+"").replaceAll("_"," ")+" !"} onClick={()=>{action()}}></Button>

		</div>
		))
