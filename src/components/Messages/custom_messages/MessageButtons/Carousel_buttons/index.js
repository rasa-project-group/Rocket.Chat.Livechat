import {h,Component}from  "preact"
import {Slider} from "../Slider"
import {  memo } from '../../../../helpers';
import { update_insert_hide} from "../../MessageOffers/Actions/methodes/Offer_actions"
import {custom_Styles} from "../Button/style"
import {Custom_Button} from "../Button"
/**
 * @param Buttons=[{"title":String,"payload":""}]
 */
export const Button_carousel = memo(
	({
		action,
		Buttons,
		msg,

	}) => (
				<div  style={{height:"100px"}} >
				<Slider options={{
					  autoPlay: false,


					}} >
{Buttons.map((element,i)=>{
					 return(<Custom_Button Style={custom_Styles.Carrousel_button_style()} action={action} element={element}></Custom_Button>)
				 })
				 }
				</Slider>
				</div>
	))


