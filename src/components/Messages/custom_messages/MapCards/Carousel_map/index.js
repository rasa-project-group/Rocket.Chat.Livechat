import {h,Component}from  "preact"
import {Slider} from "../../MessageButtons/Slider"
import {  memo } from '../../../../helpers';
import {Card_Map} from "../Maps"
/**
 * @param Locations=[{"Dest":{},"Duration":{}}}]
 */
export const MAP_carousel = memo(
	({
		action,
		Locations,
		msg
	}) => (
				<div   style={{marginTop:"10px",marginBottom:"60px", }} >
				<Slider options={{
					  autoPlay: false,

					}} style={{margin:"0%"}} >
    {Locations.map((element,i)=>{
					 return(<Card_Map   element={element}></Card_Map>)
				 })
				 }

				</Slider>
				</div>
	))
