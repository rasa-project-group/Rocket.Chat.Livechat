import { h , Component } from "preact";
import { render } from "preact/compat";
/** @jsx jsx */
import { css, jsx } from '@emotion/react'


import {

	Card,
	CardPrimaryAction,
	CardActions,
	CardMedia,
	CardActionButton,
	CardActionIcons,
	CardActionIcon,
	CardActionButtons,
} from "@rmwc/card";
import { Typography } from "@rmwc/typography";
import { createClassName, memo } from '../../../../helpers';
import {store} from "../../../../../store"
import {Card_style} from './styles'
import {open_map_window} from "../../lib/Shared_functions"
import "@rmwc/card/styles";
import "@rmwc/typography/styles";

/**
 *
 *
 * @param {*} {
 * 		id,
        mapcard
 * {Dest,Origin,distance:{"text":""},duration:{"text":""},location:{lat,lng}}
 * 	}
 */

export const Card_Map = memo(

	({
		action,
		element,
		carousel_hide_action,
	}) => {
			return (
				<div style={{ width: "300px" ,marginLeft:"15px"  }}>
					<Card>
						<CardPrimaryAction>
							<CardMedia
								sixteenByNine

								css={Card_style.CardMedia_style({ Adress_Dest: element.Dest })} />
							<div style={{ display: "flex", flexDirection: "row", flexWrap: "wrap", justifyContent: "flex-start" }}>
								<div style={{ padding: "0 1rem 1rem 1rem", display: "flex", flexDirection: "column", flexWrap: "wrap" }}>
									<Typography use="headline6">Origin</Typography>
									<Typography use="body1">
										{Card_style.Destination_split(element.Origin, 2, 3)}			</Typography>
									<Typography use="headline6">	Destince 		</Typography>

									<Typography
										use="body1"

									>
										{Card_style.Destination_split(element.Dest, 1, 4)}
									</Typography>
								</div>

								<div style={{ padding: "0 1rem 1rem 1rem", display: "flex", flexDirection: "column" }}>
									<Typography use="headline6">Duration</Typography>
									<Typography
										use="body1"
									>
										{element.Duration.text}			</Typography>
									<Typography use="headline6">	Distince 		</Typography>
									<Typography
										use="body1"

									>
										{element.distance.text}
									</Typography>
								</div>
							</div>
						</CardPrimaryAction>
						<CardActions style={{ display: "flex", justifyContent: "center", flexDirection: "row" }}>
							<CardActionButtons>
								<CardActionButton onClick={() => { open_map_window(element.Dest); } }>View map</CardActionButton>
							</CardActionButtons>
						</CardActions>
					</Card></div>
			);
		}
);





