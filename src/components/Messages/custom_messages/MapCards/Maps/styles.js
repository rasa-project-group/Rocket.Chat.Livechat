import {css} from '@emotion/react'

const KEY="AIzaSyDgl46TWaF4gjo6B3ogxaMrfcDvKw4oJSk"
export const  Card_style={
	/**
	 * The function will set the static image map into The Card Media
	 * @param {*} Adress_Dest The Destination adress (e.GHJNB+45 NEW YORK,Ny)
	 * @returns
	 */
 CardMedia_style:({ Adress_Dest=""})=>{

	let dest=(Adress_Dest.replace("+","%2B")).replaceAll(" ",",")
    let alt="https://s3-eu-west-1.amazonaws.com/gbb-prod-static-data/gbb-blogs/wp-content/uploads/2016/05/06080054/Island-Lake-Bled.jpg"

	let url=`https://maps.googleapis.com/maps/api/staticmap?center=${dest}&zoom=16&size=400x400&key=${KEY}&sensor=false`

    console.log(url)

	return css`
	background-image: url(${url});


	`
},
/**
 * This function will return The destination string without "GHJNB+45"
 * @param {String} Dest The destination (e.GHJNB+45 NEW YORK,Ny)
 * @returns
 */
Destination_split:(Dest="",start,end)=>{
if(!Dest  || Dest=="" || start==0){
	return Dest
}

let slice_distance=Dest.split(" ")
let final_string=""
slice_distance.forEach((element,i)=>{
	if(i!=0 && i>=start&& i<=end){
		final_string+=element
	}
})
return final_string
}

}
