import { h , Component } from "preact";
import { render,Te } from "preact/compat";
/** @jsx jsx */
import { css, jsx } from '@emotion/react'


import {

	Card,
	CardPrimaryAction,
	CardActions,
	CardMedia,
	CardActionButton,
	CardActionIcons,
	CardActionIcon,
	CardActionButtons,
} from "@rmwc/card";
import { Typography } from "@rmwc/typography";
import { createClassName, memo } from '../../../../helpers';
import {send_card_offer_payload,update_insert_hide} from "../Actions/methodes/Offer_actions"
import {store} from "../../../../../store"
import {Card_style} from './styles'
import {open_new_window} from "../../lib/Shared_functions"
import "@rmwc/card/styles";
import "@rmwc/typography/styles";

/**
 *
 *
 * @param {*} {
 * 		id,
 * 		compact,
 * 		reverse,
 * 		use: Element = "div",
 * 		className,
 * 		style = {},
 * 		children,
 * 		system = false,
 * 		...offer
 * {offer_title,offer_description,offer_subtitle,offer_Image_path,offer_payload,ref}
 * 	}
 */

export const Card_offer = memo(

	({
		action,
		offer,
		carousel_hide_action,
	}) => (

		<Card style={{ width: "15rem" }}>
			<CardPrimaryAction>
				<CardMedia
					sixteenByNine

                   css={Card_style.CardMedia_style({backgroundimage:offer.offer_Image_path})}

				/>
				<div style={{ padding: "0 1rem 1rem 1rem" }}>
					<Typography use="headline6" tag="h2">
						{offer.offer_title}
					</Typography>
					<Typography
						use="subtitle2"
						tag="h3"
						theme="textSecondaryOnBackground"
						style={{ marginTop: "-1rem" }}
					>
                 {offer.offer_subtitle}					</Typography>

				<div css={Card_style.CardDescription_style({line_number:2})}>{offer.offer_description}</div>
				</div>
			</CardPrimaryAction>
			<CardActions>
				<CardActionButtons>
					<CardActionButton  onClick={ ()=>{ update_insert_hide(()=>{},()=>{},open_new_window(offer)) } }>show more</CardActionButton>
					<CardActionButton>Bookmark</CardActionButton>
				</CardActionButtons>
			</CardActions>
		</Card>
	)
);





