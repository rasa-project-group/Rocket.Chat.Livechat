import {h, Component} from 'preact';
import ReactCardCarousel from "react-card-carousel";
import {Card_offer} from "../Card_offer"
import '@rmwc/card/styles';
import {  memo } from '../../../../helpers';
import { PureComponent } from 'preact/compat';
function CONTAINER_STYLE() {
	return {
		height: "100vh",
		width: "100%",
		display: "flex",
		flex: 1,
		justifyContent: "center",
		alignItems: "middle"
	};
	}


/**
 *
 *
 * @param {*} {
 *
 * 	id,
 * 	compact,
 * 	reverse,
 * 	use: Element = 'div',
 * 	className,
 * 	style = {},
 * 	children,
 * 	system = false,
 * 	...offers=[] offer {offer_title,offer_description,offer_subtitle,offer_Image_path} }
 * }
 */
export class Offers_list extends PureComponent{
static get CONTAINER_STYLE() {
    return {
      position: "relative",
      height: "250px",
      display: "flex",
      flex: 1,
      alignItems: "middle",
			marginTop:"10px",
			justifyContent:"center",
			marginBottom:"80px",

    };
  }
  static get CARD_STYLE() {
    return {

      height: "250px",
      paddingTop: "80px",
      textAlign: "center",
      background: "#52C0F5",
      color: "#FFF",
      fontFamily: "sans-serif",
      fontSize: "12px",
      textTransform: "uppercase",
      borderRadius: "10px",
      boxSizing: "border-box"
    };
  }
 Hide_Carousel=(hidden)=>{
	console.warn("hiden execute")
if(hidden===Boolean)
{
this.setState({
	hide_carousel:hidden
})
}
else{
	this.setState({Carousel_visible:true})}
}
constructor(props){
super(props)
this.state={offers:this.props.offers,Carousel_visible:false}
};
render(){
	return(
		<div    style={Offers_list.CONTAINER_STYLE}>
		<ReactCardCarousel   autoplay={false} >
		 {this.state.offers.map((element,i)=>{
			 return(<div ><Card_offer carousel_hide_action={this.Hide_Carousel} action={  this.props.Card_action} offer={element}></Card_offer></div>)
		 })
		 }
		</ReactCardCarousel>

		</div>	)
}
}
