export function open_new_window(offer){

	if(!offer){
		return
	}
	if(!offer.offerRef_path){
		return
	}
	if(offer.offerRef_path==""){
		return
	}
	console.log(offer)
	window.open(offer.offerRef_path)

}



export function open_map_window(plusCode=""){
	if(!plusCode){
	return
	}
	if(plusCode==""){
		return
	}
	let UrlplusCode=(plusCode.replace("+","%2B")).replace(" ","+")
	const Googlemap_url="https://www.google.com/maps/place/"+UrlplusCode
	window.open(Googlemap_url)

}


/**
 * this function will open jitsi room
 * @param {*} Call_video :{domain: "",provider: "",rid: "",room: "",timeout: "2021-06-03T18:15:26.788Z"}
 */
export function open_jitsi_window(Call_video){
	if(!Call_video){
		return
	}
	if(!Call_video.success){
		return
	}
	console.log(Call_video.videoCall.domain,"dqsdqsd")
	let full_url="https://"+Call_video.videoCall.domain+"/"+Call_video.videoCall.room
	window.open(full_url)

}
