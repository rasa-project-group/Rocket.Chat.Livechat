import {Livechat} from "../../../../api"
import {open_jitsi_window} from "./Shared_functions"

/**
 * this function
 * @param {*} rid room id
 * @returns call_info:{domain: "",provider: "",rid: "",room: "",timeout: "2021-06-03T18:15:26.788Z"}
 */
export async  function Videocall(rid){
    if(!rid){
		return
	}
    /**
	 *
	 */
	return await Livechat.videoCall({rid:rid}).then((call_info)=>{
		console.log("call_info",call_info)
        open_jitsi_window(call_info)
	}).catch((ex)=>{
		console.log(ex.msg)



	})

}
