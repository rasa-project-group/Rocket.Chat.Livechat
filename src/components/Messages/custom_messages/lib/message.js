import { Livechat } from '../../../../api';
import store from        '../../../../store';
import { createToken, debounce, getAvatarUrl, canRenderMessage, throttle, upsert } from '../../../helpers';

export async function Send_message(msg,options={_hidden:false}){
	if (msg.trim() === '') {
		return;
	}
	await grantUser();
	const { token, user,room } = store.state;

	console.log("rooms",room)

	try {

		await Promise.all([
			Livechat.sendMessage({ msg, token, rid:room._id,_hidden:options._hidden}),
		]);
	} catch (error) {
		const reason = error?.data?.error ?? error.message;
		const alert = { id: createToken(), children: reason, error: true, timeout: 5000 };
	}
	await Livechat.notifyVisitorTyping(room._id, user.username, false);


}

export async function  Update_message(message,newmessage,options={}){
	console.log(newmessage._id+"id")

if(!(message._id==newmessage._id )){
return

}
console.log(newmessage._id+"id")

if(options._hidden){
	message._hidden=options._hidden
}
const { token } = store.state;

const{rid,msg}=message
console.log(rid)
await grantUser();
try {
	await Promise.all([
		Livechat.hidmessagebot(message._id,{token:token,rid:rid,msg:msg,_hidden:message._hidden,agent:{agentId:message.u._id,username:message.u.username}})
	]);
} catch (error) {
	const reason = error?.data?.error ?? error.message;
	const alert = { id: createToken(), children: reason, error: true, timeout: 5000 };
	console.log(error)
}

}



async function  grantUser ()  {
	const { token, user, guest, dispatch } = 	store.state;

	if (user) {
		return user;
	}

	const visitor = { token, ...guest };
	const newUser = await Livechat.grantVisitor({ visitor });
	await dispatch({ user: newUser });
}
