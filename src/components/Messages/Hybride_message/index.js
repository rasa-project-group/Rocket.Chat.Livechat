
import { h, Component } from "preact";

import {Offers_list} from '../custom_messages/MessageOffers/Card_offer_list/index'
import store from '../../../store';
import {update_card_offer_actions} from "../custom_messages/MessageOffers/Actions/methodes/Offer_actions"
import {MAP_carousel} from "../custom_messages/MapCards/Carousel_map"
import {Button_carousel} from "../custom_messages/MessageButtons/Carousel_buttons"
import {Custom_link_Button} from "../custom_messages/MessageButtons/Button"
import {Videocall} from "../custom_messages/lib/videoCall"
import  {custom_Styles} from "../custom_messages/MessageButtons/Button/style"
export const attachments_enum = {
	BUTTONS: "buttons",
	OFFERS: "offers",
	SIMULATIONS:"simulations",
	SUMULATIONS_SYBTYPES:"simulationsubtypes",
	MAP_CARD:"mapcard",
	CALL_BUTTON:"VIDEO_CALL_BUTTON"

}
export const Jitsi_method_id={
 JOIN:"joinJitsiCall"
}

export function Hybride_Attachement_Message(message_item,Attechements={"attachments_type":"","attachments_data":[]}){

    const {token,} = store.state;
    console.log(token)
    console.warn(Attechements.attachments_data)
    console.log(token)
    if(Attechements.attachments_data){
        switch(Attechements.attachments_type){

            case attachments_enum.OFFERS:               { console.table("Offers",Attechements.attachments_data); return <Offers_list  Card_action={ ()=>{update_card_offer_actions(message_item)}} offers={Attechements.attachments_data}></Offers_list>} break;
            case attachments_enum.BUTTONS:              { console.table("buttons",Attechements.attachments_data);return<Button_carousel action={()=>{update_card_offer_actions(message_item)}} Buttons={Attechements.attachments_data} ></Button_carousel> }break;
            case attachments_enum.SIMULATIONS:          { console.table("simulations",Attechements.attachments_data);return<Button_carousel action={()=>{update_card_offer_actions(message_item)}} Buttons={Attechements.attachments_data} ></Button_carousel> }break;
            case attachments_enum.SUMULATIONS_SYBTYPES: { console.table("simulationsubtypes",Attechements.attachments_data);return<Button_carousel action={()=>{update_card_offer_actions(message_item)}} Buttons={Attechements.attachments_data} ></Button_carousel> }break;
            case attachments_enum.MAP_CARD:             { console.table("mapcard",Attechements.attachments_data); return <MAP_carousel Locations={Attechements.attachments_data}  ></MAP_carousel>}
            default: return null ;

        }
	}



}
/**
 * This function will extract the actions links and return Button or null =>  join to the jitsi room
 * @param {*} Action_link    Array Of actions [{ icon: 'icon-videocam', label: TAPi18n.__('Click_to_join'), i18nLabel: 'Click_to_join', method_id: 'joinJitsiCall', params: ''}]
 * @returns Buuton
 */
export function Hybrider_Action_link_message(message_item,Action_link=[]){

if(Action_link.length==0||! message_item  ){
	return null
}
if(Action_link.length==1){
	let action_link=Action_link[0]
	if(!action_link.method_id){return null} ;
    if(action_link.method_id==Jitsi_method_id.JOIN){
		const {token,room} = store.state;
        if(!room._id){return null}
        return  <div><Custom_link_Button Style={custom_Styles.Div_Button_link()} action={()=>{Videocall(room._id)}} element={action_link} ></Custom_link_Button></div>
	   }
}
else {
	return null
}


}

