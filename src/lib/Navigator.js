import {store} from '../store'
import {Livechat} from "../api"
import {Send_message } from "../components/Messages/custom_messages/lib/message"
import {Success_location,Faild_location } from "./locationmessage"
export function geolocalistaion(){
	if(navigator.geolocation){
	navigator.geolocation.getCurrentPosition((position)=>{
		store.setState({cordinate:{
         latitude:position.coords.latitude,
		 longitude:position.coords.longitude,

		}})
		let success_message=Success_location(position. coords)
		Send_message(success_message,{_hidden:true})
	},

	(error)=>{
		let faild_message= Faild_location(error)
        Send_message(faild_message,{_hidden:true})

	})

	}
	else {
		return false
	}

	}

