# Rocketchat LiveChat ⭐

This is a customized version of the original Rocketchat Livechat [Rocketchat.livechat](https://github.com/RocketChat/Rocket.Chat.Livechat)

This version it is using [BrowserSyncPlugin](https://www.npmjs.com/package/browser-sync-webpack-plugin) to automatically restarting the [Webpack](https://www.npmjs.com/package/webpack) building operation for any change made in the files 
## Cli Commands


```bash
# install dependencies
yarn

# serve with hot reload at localhost:8080
yarn dev

# build for production with minification
yarn build

# test the production build locally
yarn serve

# run tests with jest and preact-render-spy
yarn test

# run the storybook
yarn storybook

# before commit run
yarn i18n

```

## Usage

##### 

```node
<script>
		(function(w, d, s, u) {
			w.RocketChat = function(c) { w.RocketChat._.push(c) };
			w.RocketChat._ = [];
			w.RocketChat.url = u;
			var h = d.getElementsByTagName(s)[0],
				j = d.createElement(s);
			j.async = true;
			j.src = 'http://localhost:5000/rocketchat-livechat.min.js?_=' + Math.random();
			h.parentNode.insertBefore(j, h);
		})(window, document, 'script', 'http://localhost:5000');

		RocketChat(function() {
			this.setCustomField('key-test', 'value test');
		});
	</script>
```



## License
